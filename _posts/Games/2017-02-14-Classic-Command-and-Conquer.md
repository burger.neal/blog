---
layout: post
current: post
cover:  assets/images/posts/Games/2017-02-14-Classic-Command-and-Conquer.jpg
navigation: True
title: Command and Conquer Tiberium Dawn / Red Alert / Dune 2000
date: 2017-02-14 17:39:00
tags: [Games]
class: post-template
subclass: 'post'
author: happyneal
---

Command and Conquer, was one of the greatest Strategy Series that was ever created.
A while ago the older Games have been released for free.

The official CDs and How to install you can find [here](http://www.cncworld.org/index.php?page=features/tutorials/index). 

However it is easier to just install from [cncnet](https://cncnet.org/).

# Red Alert
Fun additional stuff for Red Alert can be downloaded at:

[Campaigns](http://ra.afraid.org/html/downloads/campaigns.html)

Getting Red Alert to run on Windows 7 with correct colors:

1. Go to [http://hifi.iki.fi/cnc-ddraw/#download](http://hifi.iki.fi/cnc-ddraw/#download)
2. Copy the .dll file into the Red Alert Directory

If that is not enough, take a look at

# Dune 2000 Grunt Mod

* [Dune2000](https://gruntmods.com/dune_2000_gruntmods_edition/)
* [Dune 2000 Mods](https://d2kplus.com/)

# Open RA

* [OpenRA](http://www.openra.net/)
