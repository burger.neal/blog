---
layout: post
current: post
cover:  assets/images/posts/Games/2015-02-14-Aperture-Science-Investment-Opportunity.jpg
navigation: True
title: Aperture Science Investment Opportunities
date: 2015-02-14 12:39:00
tags: [Games]
class: post-template
subclass: 'post'
author: happyneal
---

Aperture Science Technologies is a leading tech company. You definitely should invest in their breakout technologies.
They were created with Science. Are top of the line and much better than anything that Black Mesa has to offer.


# Product line
Cave Johnson will now present you with their current product line.

## Good old fashioned Panels

<iframe width="560" height="315" src="https://www.youtube.com/embed/0qcED35LL8I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Trust in AI

<iframe width="560" height="315" src="https://www.youtube.com/embed/AZMSAzZ76EU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Turrets

<iframe width="560" height="315" src="https://www.youtube.com/embed/6i-nMWgBUp0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Boots
<iframe width="560" height="315" src="https://www.youtube.com/embed/wX9Sc88qreg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

# Multiverse
Not to forget that Aperture Science is almost bankrupt and is running the most elaborate con game in the multiverse:
<iframe width="560" height="315" src="https://www.youtube.com/embed/b7rZO2ACP3A" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

So go and help out Aperture Science.