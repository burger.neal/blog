---
layout: post
current: post
cover:  assets/images/posts/Games/2015-12-25-StarTrekOnline.jpg
navigation: True
title: Star Trek Online (F2P)
date: 2015-12-25 17:39:00
tags: [Games]
class: post-template
subclass: 'post'
author: happyneal
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/gqed10FOmL4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

A MMORPG for the Star Trek fans. It lets you visit and explore the entire Star Trek Universe. However it is also suited for people that haven't heard anything about star trek and provides extensive back stories and information about the different alien races, the history of the universe etc.

# Game 
Well it is split into Space Sections (yey) and Ground Sections (meh).  As you level up for every 10 levels you get a newer and bigger ship. Quests are divided into Daily Quests and Episode Quests. Completing the Episode Quests allows you to level extremely fast. In the middle of the Episodes it just becomes repetitive and boring. However Reaching the Undine Quests it does provide again diversity and better quests. Especially the current "The 2800" Featured Episode is very well made. (And even resolves a Plot-hole left by Star trek Deep Space Nine)

Sadly the "Exploration" part is rather dull and beaming down to some uninhabited planet to find in a square mile 5 debris of some crashed probe is just no fun.

Most of the game you can play in the Single player mode. However some parts (especially Fleet actions) are designed for min. 5 players (yey find 4 friends to play star trek) - however you can easily auto team and play with a lot of people that blow everything up without any tactic.

# End Game:
Well after completing the episodes, there is not really anything to do except to wait for new episodes. You can collect several Item Sets (the Breen Set, Borg Set, Omega Force, MACO etc.) However you are only allowed

# Free To Play
That the game is free to play does not limit you in anyway to play the entire game. However you can buy a lot of items as well as a new big ship when you reached Level 50.

# Overall
I had a great time playing the game. Really liked all the small details of the lush environment and the strange new worlds. It is a nice star trek game and definitively worth a look.

* [Official Site](http://www.startrekonline.com/)
* [Steam](http://store.steampowered.com/app/9900/)
* [Tips and Tricks](http://www.stowiki.org/Main_Page)