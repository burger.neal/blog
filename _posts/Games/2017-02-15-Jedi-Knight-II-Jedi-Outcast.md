---
layout: post
current: post
cover:  assets/images/posts/Games/2017-02-15-Jedi-Knight-II-Jedi-Outcast.jpg
navigation: True
title: "Jedi Knight II: Jedi Outcast"
date: 2017-02-15 17:39:00
tags: [Games]
class: post-template
subclass: 'post'
author: happyneal
---
<iframe width="560" height="315" src="https://www.youtube.com/embed/biXe7iv_rJA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Finally I got the chance to revisit Jedi Knight II. So does it hold up after all these years or is it only great because of nostalgia?

I forgot how frustrating the first couple of levels are. The Imperial Blaster works just like in the Star-wars Film - only every third shot actually hits the target you are aiming at. The levels are designed as mazes and you need to solve small puzzles.

After the first couple of levels the story kicks into gear, your partner is killed by a Dark Jedi and you want revenge. A quick force Training level and you got your light-saber back.

Then finally the first level that you get your light-saber and can slice up some thugs in a bar? No, you learn the hard lesson, that you cannot block sniper shots and even if you only see two pixels of an enemy's head a disintegration shot kills you immediately. Well "Quick-Save" and "Quick-Load" is your friend I suppose...

After you survive that stupid level, it is actually quite nice, got to visit cloud city and over time the game gets better due to the fact that you start "learning" new force skills, making light-saber battles much more easier. They even try to recreate Iconic Scenes from the Empire strikes back.

Overall the game has its issues, while I really like FPS that are more maze like and promotes exploration. However the game puts in a lot of effort to tell the story of Kyle Kartan and only uses minor iconic elements from the movie making it a really nice extension of the Star Wars universe.

The nostalgia effect is there, it was one of the first games that made light-saber battles look good and possible (previously normal weapons were the better choice and you only used the light-saber as last option.

Next Up: Jedi Academy...

# Store:
* [GOG](https://www.gog.com/game/star_wars_jedi_knight_ii_jedi_outcast)
* [Steam](https://store.steampowered.com/app/6030/STAR_WARS_Jedi_Knight_II__Jedi_Outcast/)