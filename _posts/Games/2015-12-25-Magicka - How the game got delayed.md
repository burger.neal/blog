---
layout: video
current: video
cover:  assets/images/posts/Games/2015-12-25-Magicka.jpg
navigation: True
title: Magicka - How the game got delayed
date: 2015-12-13 13:39:00
tags: [Games, Video]
class: video-template
subclass: 'post'
author: happyneal
---
The classic retelling of how software is being developed.

<iframe width="560" height="315" src="https://www.youtube.com/embed/OE5fGjJ3Myg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Part II: Is always better than Part I

<iframe width="560" height="315" src="https://www.youtube.com/embed/eRLRwU-RTT0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
