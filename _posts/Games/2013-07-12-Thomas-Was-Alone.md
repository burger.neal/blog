---
layout: post
current: post
cover:  assets/images/posts/Games/2013-07-12-Thomas-Was-Alone.jpg
navigation: True
title: Thomas Was Alone
date: 2013-07-12 12:39:00
tags: [Games]
class: post-template
subclass: 'post tag-getting-started'
author: happyneal
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/5K4zjNtQ3y8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

What can you really do with three simple shapes? 

As it turns out you can do quite a lot with them. Thomas was alone gives each rectangle superpowers. Then throws them into beautifully designed minimalist environments, challenges them with difficult puzzles and gives them a very interesting story to work with.

While you are playing this jump and run puzzler you will be hearing a beautiful soundtrack and a perfect narrator that will pull you in and make you feel compassion with these simple shapes that are jumping around on your screen. 

Even though you are playing with simple rectangles it is a well rounded game.

Go get it in the 
* [Steam Store](http://store.steampowered.com/app/220780/)
* [mike bithell games](http://www.mikebithellgames.com/thomaswasalone/)
