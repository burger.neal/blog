---
layout: post
current: post
cover:  assets/images/posts/Games/2017-02-15-Jedi-Knight-Jedi-Academy.jpg
navigation: True
title: "Jedi Knight: Jedi Academy"
date: 2017-02-15 17:39:00
tags: [Games]
class: post-template
subclass: 'post'
author: happyneal
---
<iframe width="560" height="315" src="https://www.youtube.com/embed/YZu0aYR5k_Y" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Kyle Kartan keeps his force Powers after the Events of Jedi Knight II and what is the best way to teach new Students into the ways of the force? give them a light-saber and let them slaughter a lot of people. So well maybe in the educational department the game is slightly laking...

Essentially the Game is a glorified mission pack for Jedi Knight II and adds a slightly better combat system and dual lightsabers and doublesided lightsabers.

Ultimately it does not matter what force powers you use "it is how you use them" that causes you to go to the dark or light side. However in realty the only thing that  influences your fall to the dark side is to kill off that annoying npc that anyway was seduced to the dark side. (Well if you don't kill him he is saved blabla). But you probably should kill him, simply because it makes the last couple of levels more interesting, as you have the double amount of lightsaber wielding enemy's - everybody hates you, that's justification enough to slaughter them all and become the most powerful dark lord of all time.

Overall it is a very lacking game, the only redeeming factor is actually its multiplayer... However apparently the times of lan-parties are over... so sadly the games only redeeming factor are the good old memories.

# Store:
* [GOG](https://www.gog.com/game/star_wars_jedi_knight_jedi_academy)
* [Steam](https://store.steampowered.com/app/6020/STAR_WARS_Jedi_Knight__Jedi_Academy/)