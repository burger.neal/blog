---
title: "Scott Kim - Puzzle Designer"
date: "2016-12-18"
layout: post
tags:
    - "Games"
    - "Puzzles"
cover:  assets/images/posts/Games/2018-05-21-HeavenandEarth-ScottKim.gif
author: happyneal
---

One of the first computer games I have ever played was Heaven and Earth. It was divided into three different parts: Illusions Card Games and the Pendulum.

Especially the Puzzle Section got me hooked. It had the most interesting and challenging Puzzles I have ever seen in a computer game and still outshine many puzzles that you would 

# Puzzle Master - Scott Kim

<iframe width="560" height="315" src="https://www.youtube.com/embed/EHxR9kBVVV4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

> What I want is to have several little 'aha' moments where your brain is really happy. (Scott Kim)


# Heaven and Earth
[Heaven and Earth](https://www.myabandonware.com/game/heaven-earth-1fb/play-1fb)

## Passwords
If you own Heaven & Earth but have lost the manual, and therefore can't get past the initial password protection screen where it asks you questions about the game, here are all the answers you need to know.

INTRODUCTION:
What is the last name of the founder of the Naropa Institute?
**TRUNGPA**

INTRODUCTION:
Who was the first ruler of Shambhala?
DAWA SANGPO

PENDULUM: POSITIVE VORTICES
in the Sky level are multi-colored & look like what?
STARBURSTS

PENDULUM: POSITIVE VORTICES
in the Ocean level are blue & look like what?
PEARLS

CARD GAME:
What Japanese card game inspired Heaven & Earth's card game?
HANAFUDA

CARD GAME/SUITS/ELEMENTS:
May, September, and January are in which element?
EARTH

CARD GAME/SUITS/ELEMENTS:
June, October, and February are in which element?
AIR

CARD GAME/SUITS/ELEMENTS:
April, August, and December are in which element?
FIRE

CARD GAME/SUITS/ELEMENTS:
July, November, and March are in which element?
WATER

CARD GAME/CREATING TRICKS/OPPOSITE MONTH:
What does June + December represent?
SOLSTICE

CARD GAME/CREATING TRICKS/OPPOSITE MONTH:
What does March + September represent?
EQUINOX

CARD GAME/TRICK SCORES:
What opposite month trick is worth 800 points?
MOUNTAIN

CARD GAME/CELESTIAL PHENOMENA:
What Weather phenomenon has a multiplier of -3?
TORNADO

CARD GAME/CELESTIAL PHENOMENA:
What Stars phenomenon has a multiplier of 5?
SUPERNOVA

ILLUSIONS/CURSOR WARPING/LEVELS:
What is another name for the Ocean level?
VELOCITY

ILLUSIONS/CURSOR WARPING/LEVELS:
What is another name for the Desert level?
SYMMETRIES

ILLUSIONS/MULTIPLE CURSORS/LEVELS:
What is another name for the Ocean level?
UNITY

ILLUSIONS/IDENTITY MAZE/LEVELS:
What is another name for the Ocean level?
PARALLEL

ILLUSIONS/CHANGING BODIES/LEVELS:
What is another name for the Ocean level?
STANDARD

ILLUSIONS/ANTIMAZE/LEVELS:
What is another name for the Sky level?
FLIP-FLOP

PILGRIMAGE:
Shambhala is about the path of what?
WARRIORSHIP
