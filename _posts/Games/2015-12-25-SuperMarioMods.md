---
layout: post
current: post
cover:  assets/images/posts/Games/2015-12-25-SuperMarioMods.jpg
navigation: True
title: Super Mario Mods
date: 2015-12-25 17:39:00
tags: [Games, Mods]
class: post-template
subclass: 'post'
author: happyneal
---

Super Mario is probably one of the most beloved games that was ever created. The Mario fanbase has created over the time 

# Super Mario Crossover:
Play as any Snes/Nes Character

* [Super Mario Crossover](http://www.explodingrabbit.com/games/super-mario-bros-crossover)

# Super Mario Tetris
Combine Tetris with Mario, simple enough

* [Super Mario Tetris](http://www.newgrounds.com/portal/view/522276)

# Super Mario Portal
Combine Super Mario with Portal and Hat generator

* [Super Mario Portal](http://stabyourself.net/mari0/)