---
layout: post
current: post
cover:  assets/images/posts/Games/2017-02-14-Bioshock-Remove-Intro-Videos.jpg
navigation: True
title: Bioshock - Remove Intro Videos (Steam)
date: 2017-02-14 17:39:00
tags: [Games]
class: post-template
subclass: 'post'
author: happyneal
---

Bioshock Remastered - Updated Graphics, but same old stupid multiple unskippable 2k Intro Video. Great…

To avoid waiting for ages to get the stupid game going. You can add a the flag “-nointro”  to the launch options.

1. Right-click on the game in the steam library and select “Properties”. 
2. Then click on the button “Set Launch Options” 
3. Add the flag "-nointro"
