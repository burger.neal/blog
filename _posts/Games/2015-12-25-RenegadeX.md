---
layout: post
current: post
cover:  assets/images/posts/Games/2015-12-25-RenegadeX.png
navigation: True
title: Renegade X - Beyond Black Dawn
date: 2015-12-25 14:39:00
tags: [Games, Mods]
class: post-template
subclass: 'post'
author: happyneal
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/jNpPQN758uc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Renegade X is a free small 1-2 hours single player game showing off some of the features of the UDK. 

Future Releases will have an Multiplayer Mode as well. The game recreates the original CnC Renegade, with many similar features like Weapons an Vehicles. 
It has way better graphics than the original and many impressive in-game cinematic. It even recreates the bad humor from the original. 
However the game doesn't feel retro, it feels like a new and improved version.

It is a very enjoyable small little game. 
Overall it is a very impressive game, and it is defiantly worth a look. 
I am looking forward to the final finished project.

The game is free and can be downloaded at [http://www.renegade-x.com/](http://www.renegade-x.com)
