---
layout: post
current: post
cover:  assets/images/posts/Games/2018-05-14-GeorgeLois-DamnGoodAdvice.jpg
navigation: True
title: George Lois - Damn Good Advice
date: 2018-05-14 08:39:00
tags: [Books]
class: post-template
subclass: 'post'
author: happyneal
---

Damn good Advice (for people with talent) is a wonderful visual book. It was created by George Lois a very successful art director for the esquire magazine.

Of course a book about advice is going to start out giving you advice about who you are as a person. 
And that you should be proud of who you are and be smart and industrious.

> Better to be reckless than careful.<br />
> Better to be bold than safe.<br />
> Better to have your work seen and remembered,<br />
> or you've struck out.<br />

I do not always like that blanket statement that people should rebel. Nobody rebels against everything you pick and choose what you rebel against. People need to have a certain amount of stability so that they have the ability to rebel against something.

As soon as you start having ideas you also need something else: Work ethik. As usual the book suggests to work so much that you drop totally exhausted into bed.
I guess that is part of being 'the best' - I do not think it is sound advice. You need to have fun while doing your work unless you want to end in some sort of burnout. It also perpetuates the idea that the only 'work' is important in life. There are other paths to success.

> Think long. Write Short.

There is nothing to add to this 

> Don't expect a creative idea to pop out of your computer.

A computer is only a tool. You first need to grasp the big idea. Then you can start improving upon that idea by using an computer. This advice is very important even for the field of computer science and programmers.

> Teamwork might build a building, but it cannot create big ideas - Everybody believes in co-creativity not me. Be confident in your own, edgy, solo talent.

Yes, but you need other people to trust that you are that 'solo' talent. Basically - just do not be an asshole. Work with other people on other parts of the project - just be the guy that has the vision.

> Enjoy your Work - be trilled about your work (and enjoy getting a paycheck for it)

When you start enjoying your work there your life does not feel anymore as if you are working but actually accomplishing something.

> Make your presence felt

Everybody should know that you exist and are an important part. You are a respectable person. The world should look up to you. Dont take the bullshit from people that do not respect you.

> No matter what work you do, do it fast and on time and do it perfectly

Deadlines are always picked randomly. They should not be used to let you slack around and then do a rush job in the end. If you got a task to do. Start doing the task.

> Most people work at keeping their job rather than doing a good job.

You will not find security in doing the same thing every day when the world is changing at a rapid pace.

> Never work for bad people

Its not good for you in anyway and you will feel depleted and question your own morals. Don't give bad people the power to continue their bad practices.

The remainder of the book is more focused on advertising or has the one or the other bad advice.

* [Amazon](https://amzn.to/2KpK59W)