---
title: "Trailer: Star Trek:Beyond"
date: "2015-02-14"
layout: post
description: "Star Trek meets Fast and the Furious"
category: "Trailer"
tags:
    - "Blog"
    - "Trailer"
cover:  assets/images/posts/Blog/2015-02-14-Trailer-Startrek-Beyond.jpg
author: happyneal
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/XRVD32rnzOw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

The next Star Trek, presented by Fast and the Furious Crew. By the looks of the Trailer we Star Trek will return to its glory days of "The Next Generation" the Captain is going to face a difficult issue that takes a profound look on humanity and how far we have come.

This time the issue is, there are too many fast and loud SciFi Action Movies... So another one, this time louder and faster than ever before... Oh well... hopefully they are not ripping of some other Star Trek movie again...