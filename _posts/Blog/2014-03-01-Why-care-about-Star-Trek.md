---
layout: post
current: post
cover:  assets/images/posts/Blog/2014-03-01-Why-care-about-Star-Trek.png
navigation: True
title: "Why care about Star Trek?"
date: 2014-03-01 17:39:00
tags: [Blog]
class: post-template
subclass: 'post'
author: happyneal
---

Star Trek - no matter if you liked the show or not. 
It has had an profound influence on your life. 
Many of the ideas presented in Star Trek were absolutely unthinkable at that time. So even if you have never seen Star Trek or don’t really like Star Trek, it would be nice if you would bear with me.

Star Trek was created 1967 by Gene Rodenberry, he envisioned a united world, where everybody was working together for a better future. There was no separation based on nationality or race. When looking back at that same time period you will notice that at the same time the African-American Civil Rights Movement was taking place in America. The stories for the show were written by visionary science fiction authors, like Harlan Ellision. Sadly the stories were "adapted" for television, in essence  "dumbed down". The stories were original and unique and famously featured the first inter-racial kiss on television. However at the time the show struggled to find viewers and the show was canceled after 3 seasons.

This could have been the end, a small unimportant television series, forgotten as time goes by. However  the show was syndicated and fans started to organize themselves and created conventions. These fans called themselves "trekkies". The show had become more and more popular over the years, in fact it had become so popular that 21 years later, a new Series was created: "Star trek: the next Generation" (1988). The journey to explore the universe continued. One thing that set the series apart from many other series was, that for many conflicts there was no obvious right or wrong resolution, only different possibilities how to handle the situation. The other thing that impressed me was that Captain Picard when facing an enemy starship, first try to negotiate and only if everything else failed he would use the ships weapons. It is interesting to note that the Cold War was taking place during that time period.

Scientists and computer geeks were inspired by the show. One computer scientist watched an episode where Data (an Android) was listening to two music pieces at once. He asked himself, how can I get my computer to do the same? - The precursor of the popular MP3-format was born. Other devices directly influenced by star trek are: The Personal Computer (a preposterous idea in the 60s), Wireless Communication (technically feasible since the 90s), Tablet Computers (2010s), Portable Storage devices (2000), Wireless Headsets and many more.

However in the spinoff-series "Deep Space Nine" (1993), especially during the end of the series the use of weapons, war stories and space battles have become more common. Deep Space Nine always tried to revisit prior stories and look at the long-term consequences of those actions, which sadly led to an all-out space war. In another spinoff, Star trek: Voyager (1995) they also increasingly rely on weapons as resolution of conflict. The first episode practically starts with blowing up a space station as the only solution. In the last episode they even get to use weapons "from the future" and it is implied that they destroy an entire alien race.

Star trek envisions a future for the human race. In the beginning it was a bright wonderful exciting future. "To boldly go where no man has gone before", this is from the beginning of the opening of Star trek. Lately, however, Star trek has not gone anywhere, especially with the two new movies Star trek (2009) and Into Darkness (2013). It has gone down the safe path to appeal to a mass audience. The stories do not discuss philosophy or science, always use violence as conflict resolution and the story does not want you to think. Have we become so pessimistic, so that we can only imagine a dark dystopia futures filled with violence and wars? Are people only entertained by explosions and attractive actors?  Star trek is a reflection of the human condition.

I am optimistic, that star trek will change and get better in the future and I hope that you also care about it. 

> "Unless someone like you cares a whole awful lot, Nothing is going to get better" (The Lorax)