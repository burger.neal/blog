---
title: "The D in David"
date: "2016-06-19"
layout: post
tags:
    - "Blog"
    - "Animation"
cover:  assets/images/posts/Blog/2016-06-19-TheDInDavid.jpg
author: happyneal
class: post-template
navigation: True
---

<iframe src="https://player.vimeo.com/video/162384039" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>