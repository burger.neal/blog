---
title: "Understanding the Rise of China"
date: "2015-12-27"
layout: post
category: "Video"
tags:
    - "Blog"
    - "Video Essay"
cover:  assets/images/posts/Blog/2015-12-27-The-Rise-Of-China.jpg
author: happyneal
class: post-template
navigation: True
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/imhUmLtlZpw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>