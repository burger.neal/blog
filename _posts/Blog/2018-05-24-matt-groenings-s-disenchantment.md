---
layout: post
title: Matt Groenings's Disenchantment
date: 2018-05-24 10:35 +0200
current: post
cover:  assets/images/posts/Blog/2018-05-24-matt-groenings-s-disenchantment.jpg
navigation: True
tags: [Blog]
class: post-template
subclass: 'post'
author: happyneal
---

Yesterday it has been revealed that the new epic fantasy series Matt Groening has been working on is called 'Disenchantment' will feature 10 episodes and start airing on Aug. 17.
The same Animation Studio 'ULULU Company' that animated Futurama will animate this new series. 

Basically it looks interesting and something to look forward to in August.

Sources: 
* [Matt Groening’s ‘Disenchantment’ Debuts Aug. 17 on Netflix](https://www.awn.com/news/matt-groenings-disenchantment-debuts-aug-17-netflix)
* [Here's Your First Look at Matt Groening's New Netflix Animated Series, Disenchantment](https://io9.gizmodo.com/heres-your-first-look-at-matt-groenings-new-netflix-ani-1826260000)

Image: [Netflix](https://netflix.com)
