---
title: "Challenge Yourself"
date: "2016-07-15"
layout: post
tags:
    - "Blog"
    - "Inspiration"
cover:  assets/images/posts/Blog/2016-07-15-Challenge-Yourself.png
author: happyneal
class: post-template
navigation: True
---

![Challenge Yourself](http://poorlydrawnlines.com/wp-content/uploads/2016/06/challenge-yourself.png)

Link to Original (and more awesome comics): http://poorlydrawnlines.com/comic/challenge-yourself/