---
title: "1 > 0"
date: "2016-07-12"
layout: post
tags:
    - "Blog"
    - "Video"
cover:  assets/images/posts/Blog/2016-07-12-OneIsGreaterThanZero.jpg
author: happyneal
class: post-template
navigation: True
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/4fsYWXrGGcE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>