---
title: "Teaser: Guardians of the Galaxy: Vol.2"
date: "2016-12-18"
layout: post
tags:
    - "Blog"
    - "Trailer"
cover:  assets/images/posts/Blog/2016-12-18-Teaser-Guardians-of-the-Galaxy-Vol-2.jpg
author: happyneal
class: post-template
navigation: True
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/wX0aiMVvnvg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>