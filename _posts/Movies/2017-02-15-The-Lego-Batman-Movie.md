---
title: "The Lego Batman Movie"
date: "2017-02-15"
layout: post
tags:
    - "Movies"
cover:  assets/images/posts/Movies/2017-02-15-The-Lego-Batman-Movie.jpg
author: happyneal
---
<iframe width="560" height="315" src="https://www.youtube.com/embed/rGQUKzSDhrg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Nobody expected great things from "The Lego Batman".
And what did we get? Probably the best version of the caped crusader since the "Dark Knight".

There is nothing that can really prepare you for the awesomeness of Lego Batman.
It includes all of batman villains, the justice league, and so much more.

If you love batman, you will love this movie.
If you want your inner child squeal with delight - this is the movie for you.

Just remember:
<iframe width="560" height="315" src="https://www.youtube.com/embed/8jutkNkVgXQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
