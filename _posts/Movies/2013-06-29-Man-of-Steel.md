---
title: "Man of Steel"
date: "2013-06-28"
layout: post
tags:
    - "Movies"
cover:  assets/images/posts/Movies/2013-06-29-Man-of-Steel.jpg
author: happyneal
---

**Caution: Spoilers ahead.**

# Trailer Man of Steel
So first let's have a look at the Trailer: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/KVu3gS7iJu4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

The trailer promises a story about a boy growing up to become superman.  is deeply conflicted  and cannot handle the burden of the world. The questions that trouble superman is where he came from etc. In addition we have glimpses of spaceships landing, and superman rising to the challenge.

In essence, the movie is a reboot of a very popular franchise. So what would I expect? Well in essence a similar movie like "Batman Begins" a new origin story, slightly darker than the original, more drama conflict and based in reality.

# Superman (1978)
So what actually happened in the first Superman movies? I haven't watched them recently so this is what I remember:

* Superman's only weakness is Kryptonite
* Clark Kent works for the Daily Planet, grew up in Kansas
* The main villain is Lex Luthor
* Some awesome Ice Cave
* Louis Lane as the Love interest

Memorable Shots:
* Superman flies into space, smiles for the camera
* Superman and Louis fly around and they kiss
* Clark Kent rips open his shirt to reveal the Superman Logo
* Superman fixes the Chinese Wall with his Laser-Constructor Vision (Just kidding, it has been a while since I have seen the original)

Overall, it was a kind of goofy movie, Clark Kent being the useless guy standing in the way and nobody cares about him, and nobody notices that always when Superman appears, mysteriously Clark Kent cannot be found anywhere. No matter the fact that his "disguise" is are simple glasses.

It is fun and entertaining, nothing to serious happens, nobody gets killed, and the bad guys are put in jail. (So they can easily escape in the next movie)

# Man of Steel (2013)
Man of Steel opens similar to "Superman I" with a lengthy sequence of stuff that is going on Krypton. This time its kind of awesome, you see plant life, creatures, and some type of breeding ground (copied from the baby crops from the *Matrix*, but whatever, there are only so many ways to build cloning facilities) and of course you see General Zod's uprising and banishment into the "Phantom Zone". Clark gets sent into space and Krypton implodes (well it explodes but well minor detail, let's get stared with the story)

From then on actually a rather interesting and well thought out way to tell the back story of Superman.  Some very nice moments when Clark is talking to his father, and finds out where he comes from etc. Louis Lane starts to do research on him after he gives her a wound sealing laser surgery. (Laser eyes for the win) They give a nice little nod to the original Superman Comics where superman only can leap over buildings i.e. jumping very far and very high and then after time he learns how to fly.

After a very strong opening the writers forgot to write a movie. Practically you could stop watching the movie from the point on when General Zod threatens Earth to extradite Superman. From then on I really cannot remember much of the movie, its kind of blurry. Superman surrenders to the humans, he tells them that he trusts them and he does not wants to do them any harm. For unknown reasons Louis has to tag along to the alien spaceship. They introduce kryptonian atmosphere (so air mixed with, never mind, never gets explained) as superman's weakness. So in essence they said, "radioactive kryptonite" thats to complicated for the viewers, lets have supermans weakness be strange air. This whole mess ends in a really long, long and over the top punching scene, on earth, where nobody can get hurt (normal air) and practically Smallville gets destroyed.  While this whole mess is going on, the humans say, aliens on earth, of course we will smash them with our mighty weapons. So they try to kill all of the aliens including superman and even tough superman is trying to protect the human population, the humans fire missile after missile at superman. But then superman saves a minor commander and suddenly the humans are like: "yeah, ok superman is nice, we trust him blindly".

Zod, after watching the last failed reboot movie *"Superman returns" *uses kryptonian technology to start terraforming earth to transform it into krypton. At the same time Superman watches the *Incredibles* and figures out, flying exactly through the alien robot is enough to destroy it. The humans watch *Star Trek 11* and figure out that red-matter is on the small baby transport capsule so they fly it directly into the other alien vessel to create a black hole, sorry I meant phantom zone vacuum cleaner. However General Zod's followers had enough time to watch *Transformers III* and *Battleship* to know that the biggest fear for all humans are slowly toppling skyscraper buildings. Also some Airplanes get destroyed, probably was a lot of fun animating those. I should mention all the different types destruction in this movie. It opens with a planet destruction and follows it up with a oil rig destruction, tornado, village destruction, city destruction, so planes are not that spectacular) but the humans manage to crash into the alien spaceship and create the Mega vacuum phantom vortex sucker, of course it only effects Kryptonians, stones, cars, but not Superman, or Louis Lane. Every thing is fine, floating Kiss for Louis - the End.

What? Oh boy another endless smack down, General Zod apparently was not sucked into the phantom zone. He gets a special +2 modifier for watching "*X-Men 1*" and "*Spiderman 2*", by first using his laservision  like cyclops to cut a skyscraper in two and the same skyscraper as before topples down a second time (this time from another angle #unbelievable #omg), then instead of flying steals the dr. octopus way of climbing up buildings to smack superman again. In the end Louis magically teleports from the edge of the destroyed city into the train station, Superman brutally murders Zod, Superman cries in the arms of Louis. Clark says hi to his mum, and starts working at the Daily Planet.

## Overall:
It's a movie that starts out very strong, and then looses the will to live. Too much stuff was crammed into the movie, most of it was unnecessary. I believe if you cut out a lot of fat and useless nonsense smackdowns it could have been a very good movie. The visuals overall are very stunning, and emotionally powerful. (However the abusive use of entirely white frames, or  blown out images should be forbidden)

I really missed Lex Luthor and Clark Kent in this movie. Clark Kent is a guy in a suit working for the Daily Planet, I do not associate some hobo rescuing people with Clark Kent. It would have been better to have him be introduced in this movie in some fashion. They should have simply used the same formula of Superman I: Lex Luthor and something to make Land more valuable, Superman II: General Zod drops by earth, and wants to conquer it. It's a reboot, so you have to put in the old stuff as well as some new stuff. (Or better yet, make something new, but that's a totally different story...)