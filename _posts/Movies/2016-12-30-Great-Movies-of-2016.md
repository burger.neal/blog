---
title: "Great Movies of 2016"
date: "2016-12-30"
layout: post
tags:
    - "Movies"
cover:  assets/images/posts/Blog/2016-11-13-StarWarsAnime.jpg
author: happyneal
---

# Doctor Strange
<iframe width="560" height="315" src="https://www.youtube.com/embed/HSzx-zryEgM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

The movies visuals are stunning, the characters are neat and it was very refreshing to have a simple comic book hero movie. One Guy and his story, no team of guys.

The MCU started to look a little stale; With Dr. Strange the door to other dimensions, other world and cosmic chaos is open and ready to be explored.

# Star Wars: Rouge One 
<iframe width="560" height="315" src="https://www.youtube.com/embed/frdj1zb9sMY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Rouge One tries to play with a different style of star-wars movie and it is great. The pacing at the beginning is quite slow, which was nice and let you really enjoy the calm visuals, while at the end with the battle it gets a little chaotic.

Ironically the most annoying part of the movie is, that it tries to remind you that it is star-wars. Oh look these two fellows from the bar, here is R2-D2. While at other times the movie blends so good you do not even notice (X-Wing Pilots are all from the original Star Wars, via re-purposed footage)

And yes in the end there is "hope". New Star Wars stories in a galaxy far far away... hopefully somebody says: we do not need the old star wars nostalgia to make a decent movie, and we will finally see a movie without a death star, darth vader or blue milk.

# Kung Fu Panda 3
<iframe width="560" height="315" src="https://www.youtube.com/embed/fGPPfZIvtCw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

The story of Po finally meeting his real father was a really bad premise, however Dreamworks pulled it off and made a really inspiring movie with a great message.

However the trailer is crap and makes the movie look bad, even though the final movie is really good. THey ended the Trilogy with a great entry.

As it is Dreamworks, they will continue Kung Fu Panda with (currently) another 3 Movies. Oh well... maybe we will be surprised by more great movies like this one.

# Deadpool
<iframe width="560" height="315" src="https://www.youtube.com/embed/gtTfd6tISfw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

What happens when you have budget limitations, a lot of passion for a project, and everybody at Fox said, fuck it - we are anyway going to loose money on the project - Thus the result is a great movie that was released at the perfect time that made a ton of money.


