---
title: "Great Movies of 2014"
date: "2014-12-31"
layout: post
tags:
    - "Movies"
cover:  assets/images/posts/Blog/2016-11-13-StarWarsAnime.jpg
author: happyneal
---

# Guardians of the Galaxy

<iframe width="560" height="315" src="https://www.youtube.com/embed/crIaEzXgqto" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Marvel presents us with a talking raccoon and a walking tree. It is an exceptionally well made comic book movie. Not much more to say about it.

# Her
<iframe width="560" height="315" src="https://www.youtube.com/embed/WzV6mXIOVl4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

"Her" is a very well made Science Fiction movie. It explores the possibilities and limitations of falling in love with an Artificial Intelligence.

# Captain America: The Winter Soldier
<iframe width="560" height="315" src="https://www.youtube.com/embed/7SlILk2WMTI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

While the Captain America: The First Avenger was well made it utterly failed on having a very stupid story.

The Winter Soldier, is very well made and while it has impressive action set pieces it has in addition a great story and asks difficult questions about using drones. Additionally some character growth for Captain America and Black Widow and by the introduction of Hydra wide-ranging effects on the Marvel Cinematic Universe. (Essentially the events of Captain America saved the second half of Marvel Agents of Shield Season 1 Episodes).

The movie ends you wanting to see more Captain America and that’s a good thing.

# Earth to Echo
<iframe width="560" height="315" src="https://www.youtube.com/embed/YJgIv_hrjdg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

The Trailer for the movie, sells you an action movie about an alien trying to go home. So essentially a retelling of the story of  E.T. The movie however the story focuses more on the friendship between the kids that found the alien.

# Gone Girl
<iframe width="560" height="315" src="https://www.youtube.com/embed/Ym3LB0lOJ0o" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Well this movie is great, weird and disturbing, In essence it is a story about several people making their lives extremely horrible.

The movie has very interesting characters a lot of unexpected twists and turns and it is very hard to tell who is really the "bad" person.

# Interstellar
<iframe width="560" height="315" src="https://www.youtube.com/embed/zSWdZVtXT7E" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Interstellar is a true cinema experience, you really need to go and watch it in a theater.

Overall it is a Science Fiction movie that is based on our current understanding on how the universe works and where earth is heading in the next 100-200 years.

# Lucy
<iframe width="560" height="315" src="https://www.youtube.com/embed/MVt32qoyhi0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

Well if you can swallow the premise of the faux fact that we only use 10% of our brain you are in for a very strange ride about a woman who gains so many supernatural abilities that she gains insight into the entire knowledge of the universe.

The movie really did not know how to end, so she dies and saves all her knowledge on a Usb-stick.