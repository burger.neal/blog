---
title: "Forgettable Movies of 2014"
date: "2014-12-31"
layout: post
tags:
    - "Movies"
cover:  assets/images/posts/Blog/2016-11-13-StarWarsAnime.jpg
author: happyneal
---

2014 was a year of not so great movies, however here are the ones I watched and practically already forgot again.

# Mr.Peabody and Sherman
A boy with a time-traveling genius dog . Great premise.

# 300: Rise of an Empire
300 with ships! - A sequel to a movie that where all characters died and left the next battle up to the imagination and told us that the battle was won. So, just go and watch 300

# Robocop
Unlikable human robot kills robots. Has nothing to do with the original and is just flat out bad.

# Jack Ryan: Shadow Recruit
Something in Russia. I dont remember, I am not sure if I watched this movie, I still have my movie ticket for it.

# Wish I was here
I did not understand the movie. No Idea what was going on. Zach Braff was in it

# The Maze Runner
Apparently there are going to be more of these, because in the end, it was just the first of many tests. So a Hunger games rip-off.

# Walk of Shame
Stupid woman does stupid and offensive stuff with a lot of stereotypes. Makes me more ashamed that I watched this movie.