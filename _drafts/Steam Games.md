# The List of Hidden Steam Games in my library
Well when my Steam Library hit 200 games, I thought hey, why not clean up a little bit and remove all the games I dont play, or never will play.

# Shooter

## Spec Ops: The Line
I got the game when I was preparing for a job interview for Yeager Studios. They wrote me a couple of emails but then never got around to actually invite me to an interview. 

The game is a bad linear shooter. The only thing that elevates this game a little above other military shooters is the story. While in a shocking twist you actually should have stopped playing the game would have been the right thing all along. So remember kids, war is bad. The end of the game is extremely preachy and looking back at it I do not understand why it got so much praise from all the critics.

# Strategy
## Gray Goo
I got the game as it was touted as a spiritual successor to Command and Conquer.

Alien Creatures are looking very alien and in a shocking twist the humans are causing problems, well and grey goo, but that takes like 5 levels until that story arc starts.

The video sequences are very refined and have really high production values. However the game itself I had the difficulty to correctly identify the different structures and units. The problem intensifies as the opposing faction does not have the same unit types but look totally different.

The mechanic of creating first hubs and then connecting the structures is also quite odd and annoying at times, as a too small hub cannot be upgraded to a bigger hub etc.

Overall I only played the first 5 missions and sadly stopped after that, I just could not care about the story.

# Indie

## Amnesia - The Dark Decent
This game was part of the potato sack.
It is probably one of the best horror games that exist. 

I dont like horror. Also I got the potato too late.

## AaAaAA!!!- A reckless Disregard of Gravity
This game was part of the potato sack.

You jump of of buildings and try to hit targets and spray graffiti on buildings.
Yeah, well no, not my type of game. Also It was extremely difficult to get that stupid potato achievement.

## Bad Hotel
Random game from an humble bundle.

You have to build a hotel and defend it from attacks... 
The game was probably designed for mobile devices and not for desktop in mind. Did not click with me.

## A.R.E.S: Extinction Agenda
A game that looks like mega-man but is only 2 hours long.

The game is too easy and there is much to little of it. It plays like a Demo. The Publishers now re-released the same game with a couple of fixes to the redundant mechanics. But it doesn't look like that the game overall has improved (as it is already a award winning game)

## Dustforce
Random game from an humble bundle.

You have a broom and you must clean up. Mechanics are neat, but overall just not my game.

## Towerfall Ascention
Random game from an humble bundle.

Seems to be only a multiplayer game.

