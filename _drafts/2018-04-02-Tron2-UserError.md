---
layout: post
current: post
cover:  assets/images/posts/Games/2015-12-25-StarTrekOnline.jpg
navigation: True
title: Tron 2.0 - User Error
date: 2018-04-02 17:39:00
tags: [Games]
class: post-template
subclass: 'post'
author: happyneal
---

Tron 2.0 is one of the greatest games ever created. Sadly it is widely underappreciated.
However a couple of very dedicated people at [lsdo](http://www.ldso.net) have ensured that the game still runs on modern systems and they even went a little further
creating two additional expansions.




