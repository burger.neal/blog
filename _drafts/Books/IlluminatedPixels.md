# Introduction

1. Aesthetics of the image 
2. Physics of light
3. Appropriate Technique

Why are you lighting?

What? What do you want to imitate?

How? How do you accomplish it?

## Workspace

| Light | Renderview |
| Top | Persp |

Before lighting always position the camera.

# Light Basics

## Point Light
Shines in all directions

Imitates a light bulb, or a star

Overused by new lighters. The light is quite inefficient.
Most lights should be replaced with Spot lights, to avoid unneeded shadow calculations.

## When is using it a good choice
* A Radial Light

if possible replace with spot light

Tip:
* Watch for Hotspots
* If Shadow are not needed turn them off
* maybe imitate the light with multiple spotlights


