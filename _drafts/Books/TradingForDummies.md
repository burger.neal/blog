# Trading for Dummies


## Part I: Gathering your tools
The Difference between Trading and Investing is that Investors hold their stocks for a long time. Traders however buy and sell with a strategy.

### Chapter 1: The ins and out of Trading Stocks
Successful Trading Characteristics

* Be hard on yourself
* Work against your natural tendencies
* Fight the urge to prove yourself
* Accept the fact that you are going to make mistakes
* Ignore emotional ties to stocks


In essence it is a strategy to manage your money. It does not matter if the decision was right or wrong. It is more important that in the longterm to manage your money correctly. Always setting stop loss orders. To minimize loss of money.

## Trading styles:

* Position trading: Find the correct trend and stick with it as long as possible.
* Short-term Swing: Trade for a couple of days
* Day trading: Holding a stock for a couple of minutes


## Money Management
Trading is a business.  As such you must think of stocks as your inventory and develop strategies to avoid loosing money.
Setting an exit price even before ever entering the position is critical to long-term success.

Basic steps for creating a Trading System

1. Design and keep a Trading Log
2. Identify trading patterns
3. Exit Strategy
4. Which Trading Strategy to use
5. Develop your own trading system
6. Testing your trading system


Website List

* [BIgCharts](www.bigcharts.com)
* www.investors.com



* www.stockcharts.com
* www.freerealtime.com


Charting Software

* www.equis.com
* www.tradestation.com
* www.equis.com
* www.qcharts.com
* www.esignal.com


Fundamental

* www.wsj.com
* www.sec.gov/edgar.shtml

Analysts

* [Standard And Poors](www.standardandpoors.com)
* www.valueline.com
* www.briefing.com
* www.investor.reuters.com
* www.morningstar.com



## Part II: Reading the Fundamentals
Nothing really of interest in this chapter

## Part III: Basic Technical Analysis
Nothing really of interest in this chapter

## Part IV: Buy/Sell Strategies
Characteristics of a successful trader

* Planning your trades
* Minimizing losses by ruthless adhering to your stop losses
* Protecting profits with trailing stops
* Exiting your position when the trend ends


You need to stick to your money management plan. Do not get emotionally attached to a specific stock.
Sell as soon as the position is going against you. (Stop loss)
Do not sell when you are profiting. (Trailing stop loss)

Manage your inventory
View trading as a business. You trade to make money
Overcome the trader dilemma. Do not get caught up in the moment, stick with your plan.
Accept that you make mistakes and move on.

Think of trading as a business
Your stocks is your inventory, you need to view your business cold hearted.
Make a decision and stick with it.

Adjust your stock selection based on the season.

Protecting your principles

* Protecting your principles comes first
* Do not let a large profit turn into a small profit
* Not let a small profit turn into a loss
* Not let a small loss turn into a large loss


Investment risks:

* Opportunity Risk: Potentially the stock you picked is growing slower than the competition
* Concentration Risk: Not divesifiying over multiple segments of the market


Trading risks

* Slippage: Hidden costs
* Poor execution risk: you do not stick to your plan
* Gap risk: over the days the market can open with a gap towards the day before

