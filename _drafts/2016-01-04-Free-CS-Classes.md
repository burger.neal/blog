---
title: "Free CS Classes"
date: "2016-01-04"
layout: post
tags:
    - "Blog"
    - "Trailer"
cover:  assets/images/posts/Blog/2015-02-14-Trailer-Startrek-Beyond.jpg
author: happyneal
---

If you want to get into the field of computer science you got to learn a lot of stuff and you got to learn a lot of new stuff the whole time.

Several american universities have created free online courses (not only for computer science). Upon completion you can get a free certificate to beef up your CV.

# edX
edX offers free classes offered by MIT, Berkley, Harvard etc. They are challenging but still designed that you can take them in your spare time. The difficulty is designed for beginners as well as more advanced students.

[http://www.edx.org/](http://www.edx.org/)

#courseRa
A very similar site to edX but offers more courses in various fields.

[https://www.coursera.org/](https://www.coursera.org/)

# Udacity
To learn about computing you can go to:

[http://www.udacity.com/](http://www.udacity.com)

**Note:** If you are currently enrolled in a university, it is possible that the certificate can be accredited to your degree in that field. Talk to your student counselor.