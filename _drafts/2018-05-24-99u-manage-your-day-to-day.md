---
layout: post
title: 99u - Manage your day-to-day
date: 2018-05-24 11:55 +0200
current: post
cover:  assets/images/posts/Books/2018-05-24-99u-manage-your-day-to-day.jpg
navigation: True
tags: [Books]
class: post-template
subclass: 'post'
author: happyneal
---

[99u](https://99u.adobe.com/) Manage your day-to-day: build your routine, find your focus & sharpen your creative mind
The book is a collection of various articles. 

# Introduction
Probably your day to day is cluttered with bad habits. You need to retool and rethink about your daily habits.
Basically your day is probably filled with things you react to, like emails. However you need to be **proactive** and do things that matter to yourself.
Take the time to rethink what you are doing on a day to day basis. Then you can make your ideas happen.

# Building a Rock Solid Routine
Hemmingway wrote 500 words a day no matter what. Great creative achievements require hundreds of hours of work.

> Building a routine is all about persistence and consistency. Don't wait for inspiration; create a framework for it.

Prioritize your creative work first and then deal with the reactive work. Emails do not need to be answered immediately.

* Use triggers. Basically stick to the same tools and surroundings and that that environment will then create your 'creative zone'.
Use then something like getting your coffee and after that do always the exact same thing. (Basically connecting preexisting habits)

* Limit your todos to a single Post it Note to avoid TODO Creep and your motivation positive

* Record every commitment that you have

Have hard limits. Basically you define when you start and end your day.

Doing something frequently vastly improves the likelihood that you will do it. Basically you improve your productivity and creativity by simply making sure that you are doing it on a regular basis.

> We are what we repeatedly do. Excellence then, is not an act, but a habit. - Aristotele






# Finding Focus in a Distracted World

# Taming your tools

# Sharpening your creative mind

# Coda: Call To action
