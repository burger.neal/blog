---
title: "Rise of the MOOCs"
date: "2016-01-04"
layout: post
tags:
    - "Blog"
    - "Trailer"
cover:  assets/images/posts/Blog/2015-02-14-Trailer-Startrek-Beyond.jpg
author: happyneal
---

Over the last year a lot of new MOOC providers and courses in all fields have been become available. Lets have a look at some of the new MOOC providers.

MOOC Providers
==============

Besides [edx.org](https://www.edx.org/), [coursera.org](https://www.coursera.org/), [udacity.com](https://www.udacity.com/) a couple of new provides have popped up:

Futurelearn
-----------

[![fl-logo](http://nealbuerger.com/wp-content/uploads/2013/12/fl-logo.png)](http://nealbuerger.com/wp-content/uploads/2013/12/fl-logo.png) Futurelearn is a MOOC system based in the UK, thus focusing primarily on UK Universities. [https://www.futurelearn.com/](https://www.futurelearn.com/)

Iversity
--------

[![logo-iversity-193d16b1922b990dc9b541bc0af00588](http://nealbuerger.com/wp-content/uploads/2013/12/logo-iversity-193d16b1922b990dc9b541bc0af00588.png)](http://nealbuerger.com/wp-content/uploads/2013/12/logo-iversity-193d16b1922b990dc9b541bc0af00588.png) Iversity is Europe-centric system. This system is especially interesting for enrolled students, some of the courses provide certificates with ECTS points. These points could theoretically be used to advance your current degree. However most universities are not happy about this development, thus you probably will have to talk with several layers of bureaucracy to get the ECTS points approved. [https://iversity.org/](https://iversity.org/)

NovoED
------

[![novoed-logo-navbar-6500e1cc360cf5c275e6b09e78685a49](http://nealbuerger.com/wp-content/uploads/2013/12/novoed-logo-navbar-6500e1cc360cf5c275e6b09e78685a49.png)](http://nealbuerger.com/wp-content/uploads/2013/12/novoed-logo-navbar-6500e1cc360cf5c275e6b09e78685a49.png) NovoED focuses primarily on business education and on team projects during the courses. This alternative approach tries to encourage more students to actually complete the course by creating an obligation towards the other students in the group. [https://novoed.com/](https://novoed.com/)

Other
-----

Not strictly a "MOOC" however high quality (free) online Education:

Codeacademy
===========

Codeacademy takes a different approach to teaching how to code. The lessons are delivered in a sidebar and you must actually code a functioning program to continue the lesson. If you are stuck, the program guides you and gives you helpful hints on how to solve the problem. Currently the classes mainly focus on programming beginners. [http://www.codeacademy.org](http://www.codeacademy.org) MOOC search engines Due to the ever-growing list of available courses, several Search Engines and lists

Mooc List
---------

What can you say, it's a list of all upcoming courses. [http://www.mooc-list.com/](http://www.mooc-list.com/)

Mooctivity
----------

a list with social/interactive features giving you an instant popularity index of all courses. [http://www.mooctivity.com/](http://www.mooctivity.com/)

Skilledup
=========

A search engine, that lists the free courses as well as paid online courses. [http://www.skilledup.com/](http://www.skilledup.com/)

Developments to watch for in 2014
=================================

Mooc.org
--------

edX is creating a new platform to enable all types of institutions to publish their own MOOC type course. [http://mooc.org/](http://mooc.org/)

XSeries (Edx.org)
-----------------

The XSeries is a series of courses, that grant you a special certificate if you complete the entire coursework. One of the Certificates is from MITx Computer Sciences. [https://www.edx.org/xseries](https://www.edx.org/xseries)